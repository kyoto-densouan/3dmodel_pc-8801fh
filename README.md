# README #

1/3スケールのNEC PC-8801mk2FH風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- NEC

## 発売時期
- PC-8801FH:1986年
- PC-8801MH:1986年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-8800%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA)
- [AKIBA PC Hotline!・大ブレイクしたPC-8801mkIISRの流れを汲み登場した「PC-8801FH/MH」、CPU速度は倍の8MHzに](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1149741.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801fh/raw/22d40ea65166f915443e9d815477ab1539078c1b/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801fh/raw/22d40ea65166f915443e9d815477ab1539078c1b/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801fh/raw/22d40ea65166f915443e9d815477ab1539078c1b/ExampleImage.png)
